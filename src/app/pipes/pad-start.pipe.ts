import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'padStart',
  standalone: true
})
export class PadStartPipe implements PipeTransform {

  transform(value: any, maxLength: number = 4, fillString: string = "0"): string {
    return ('' + value).padStart(maxLength, fillString);
  }

}
