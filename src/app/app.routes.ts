import { Routes } from '@angular/router';
import { ExercicesComponent } from './components/exercices/exercices.component';
import { ListPokemonComponent } from './components/list-pokemon/list-pokemon.component';
import { ObservablesComponent } from './components/observables/observables.component';

export const routes: Routes = [
    {
        path: 'exercices', component: ExercicesComponent
    },
    {
        path: 'pokemons', component: ListPokemonComponent
    },
    {
        path: 'observables', component: ObservablesComponent
    },
    {
        path: 'pokedex',
        loadComponent: () => import('./components/pokedex/pokedex.component').then(f => f.PokedexComponent)
    },
    {
        path: '**', redirectTo: 'pokemons'
    }
];
