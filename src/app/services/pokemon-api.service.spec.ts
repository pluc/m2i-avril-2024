import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PokemonAPIService } from './pokemon-api.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('PokemonAPIService', () => {
  let service: PokemonAPIService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(PokemonAPIService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return data', () => {
    spyOn(httpClient, 'get').and.returnValues(of([
      { id: 1, libelle: 'Toto' },
      { id: 1, libelle: 'Toto' }
    ]));

    service.getAllPokemons().subscribe(p => {
      expect(p).toBeDefined();
      expect(p.length).toBe(2);
    })
  });
});
