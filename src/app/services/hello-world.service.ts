import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelloWorldService {

  public cpt: { value: number } = { value: 0 };

  constructor() { }

  public incrementCpt(): void {
    this.cpt.value++;
  }
}
