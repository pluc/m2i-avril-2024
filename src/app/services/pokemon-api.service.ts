import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { PokemonsAPI } from '../entities/pokemon-api';

@Injectable({
  providedIn: 'root'
})
export class PokemonAPIService {

  constructor(private httpClient: HttpClient) { }

  public getAllPokemons(): Observable<PokemonsAPI> {
    return this.httpClient.get<PokemonsAPI>('https://pokebuildapi.fr/api/v1/pokemon').pipe(
      catchError(e => {
        console.error(e);
        return of([]);
      })
    )
  }
}
