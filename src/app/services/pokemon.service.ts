import { Injectable } from '@angular/core';
import { Pokemon } from '../entities/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private _pokemons: Pokemon[] = [];

  public create(pokemon: Pokemon): boolean {
    if (pokemon.id != null) {
      return false;
    }

    const maxId: number = this._pokemons.length == 0 ? 0 : Math.max(...this._pokemons.map(p => p.id ?? 0));

    pokemon.id = maxId + 1;

    this._pokemons.push(pokemon);

    return true;
  }

  public update(pokemon: Pokemon): boolean {
    const index: number = this._pokemons.findIndex(p => p.id == pokemon.id);
    if (index < 0) {
      return false;
    }

    this._pokemons[index] = pokemon;

    return true;
  }

  public delete(id?: number): boolean {
    const index: number = this._pokemons.findIndex(p => p.id == id);
    if (index < 0) {
      return false;
    }

    this._pokemons.splice(index, 1);

    return true;
  }

  public getPokemonById(id: number): Pokemon | undefined {
    return this._pokemons.find(p => p.id == id);
  }

  public getAllPokemons(): Pokemon[] {
    return this._pokemons;
  }
}
