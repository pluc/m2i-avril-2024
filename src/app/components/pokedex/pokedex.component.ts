import { Component } from '@angular/core';
import { PokemonAPIService } from '../../services/pokemon-api.service';
import { Observable } from 'rxjs';
import { PokemonsAPI } from '@entities/pokemon-api';
import { SharedModule } from '@modules/shared/shared.module';

@Component({
  selector: 'app-pokedex',
  standalone: true,
  imports: [SharedModule],
  templateUrl: './pokedex.component.html',
  styleUrl: './pokedex.component.scss'
})
export class PokedexComponent {

  public pokemonsAPI$: Observable<PokemonsAPI>;

  constructor(private pokemonAPIService: PokemonAPIService) {

    this.pokemonsAPI$ = this.pokemonAPIService.getAllPokemons();
  }
}
