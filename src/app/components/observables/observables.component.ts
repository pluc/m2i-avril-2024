import { AsyncPipe } from '@angular/common';
import { Component } from '@angular/core';
import { Observable, interval, map, mergeMap, of, take } from 'rxjs';

@Component({
  selector: 'app-observables',
  standalone: true,
  imports: [AsyncPipe],
  templateUrl: './observables.component.html',
  styleUrl: './observables.component.scss'
})
export class ObservablesComponent {

  public valeur2$: Observable<number> = of(50);

  public valeur$: Observable<number> = of(42).pipe(
    map(n => n + 1),
    map(n => n / 2),
    mergeMap(n => this.valeur2$),
    map(n => n + 3)
  );

  public loop$: Observable<number> = interval(500).pipe(
    map(() => {
      const random: number = Math.random() * 100;

      return random;
    }),
    map((result: number) => {
      console.log('émission de la valeur', result);

      return result;
    }),
    take(5),
  );

  constructor() {
    this.valeur$.subscribe(v => console.log('v=', v));

    //this.loop$.subscribe(v => console.log(v));

  }
}
