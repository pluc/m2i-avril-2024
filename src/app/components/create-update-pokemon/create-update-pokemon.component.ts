import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { PokemonService } from '../../services/pokemon.service';
import { NgIf } from '@angular/common';
import { Pokemon } from '../../entities/pokemon';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-update-pokemon',
  standalone: true,
  imports: [ReactiveFormsModule, NgIf],
  templateUrl: './create-update-pokemon.component.html',
  styleUrl: './create-update-pokemon.component.scss'
})
export class CreateUpdatePokemonComponent implements OnInit {
  public formGroup?: FormGroup;
  public submitted: boolean = false;

  public pokemon?: Pokemon;

  public get isInCreation(): boolean {
    return this.pokemon?.id == null;
  }

  constructor(
    private pokemonService: PokemonService,
    private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal) { }

  ngOnInit(): void {

    this.formGroup = this.formBuilder.group({
      numero: this.formBuilder.control(this.pokemon?.numero, Validators.required),
      nom: this.formBuilder.control(this.pokemon?.nom, [Validators.required, Validators.minLength(5)])
    });
  }

  public submit(): void {
    this.submitted = true;

    if (!this.formGroup?.valid) {
      return;
    }

    const result: any = this.formGroup?.value;

    if (this.pokemon == null) {
      this.pokemon = new Pokemon();
    }

    Object.assign(this.pokemon, result);

    const success: boolean = this.isInCreation ?
      this.pokemonService.create(this.pokemon) :
      this.pokemonService.update(this.pokemon);

    if (success) {
      this.activeModal.close();
    }
  }
}
