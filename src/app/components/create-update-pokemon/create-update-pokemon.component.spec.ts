import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdatePokemonComponent } from './create-update-pokemon.component';

describe('CreateUpdatePokemonComponent', () => {
  let component: CreateUpdatePokemonComponent;
  let fixture: ComponentFixture<CreateUpdatePokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateUpdatePokemonComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreateUpdatePokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
