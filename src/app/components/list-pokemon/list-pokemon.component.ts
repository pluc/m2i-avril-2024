import { Component } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Pokemon } from '../../entities/pokemon';
import { SharedModule } from '../../modules/shared/shared.module';
import { PokemonService } from '../../services/pokemon.service';
import { CreateUpdatePokemonComponent } from '../create-update-pokemon/create-update-pokemon.component';

@Component({
  selector: 'app-list-pokemon',
  standalone: true,
  imports: [SharedModule],
  templateUrl: './list-pokemon.component.html',
  styleUrl: './list-pokemon.component.scss'
})
export class ListPokemonComponent {

  public pokemons: Pokemon[];

  constructor(private pokemonService: PokemonService, private ngbModal: NgbModal) {
    this.pokemons = pokemonService.getAllPokemons();
  }

  public create(): void {
    this.update();
  }

  public delete(pokemon: Pokemon): void {
    this.pokemonService.delete(pokemon.id);
  }

  public update(pokemon?: Pokemon): void {
    const modalRef: NgbModalRef = this.ngbModal.open(CreateUpdatePokemonComponent, {
      animation: true,
      centered: true
    });

    const component: CreateUpdatePokemonComponent = modalRef.componentInstance;

    component.pokemon = pokemon;
  }
}
