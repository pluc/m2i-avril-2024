import { NgFor } from '@angular/common';
import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

export interface IMenuRoute {
  path: string;
  libelle: string;
}

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [RouterModule, NgFor],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss'
})
export class MenuComponent {

  public menuRoutes: IMenuRoute[] = [
    { path: 'exercices', libelle: 'Exercices' },
    { path: 'pokemons', libelle: 'Pokémons' },
    { path: 'observables', libelle: 'Observables' },
    { path: 'pokedex', libelle: 'Pokédex' }
  ]
}
