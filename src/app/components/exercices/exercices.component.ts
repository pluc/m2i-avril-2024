import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HelloWorldService } from '../../services/hello-world.service';
import { ProgressBarComponent } from '../progress-bar/progress-bar.component';
import { SliderComponent } from '../slider/slider.component';

@Component({
  selector: 'app-exercices',
  standalone: true,
  imports: [
    SliderComponent,
    ProgressBarComponent,
    // Pour avoir NgIf, NgFor...
    CommonModule,
    // Pour avoir NgModel
    FormsModule
  ],
  templateUrl: './exercices.component.html',
  styleUrl: './exercices.component.scss',
  providers: [HelloWorldService]
})
export class ExercicesComponent {

  public sliderValues: { value: number }[];

  public inputValue?: string;

  public cpt: { value: number };

  public bieres: { nom: string }[] = [
    { nom: 'Kro' },
    { nom: 'Punk IPA' }
  ]
  public entiers: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

  constructor(private helloWorldService: HelloWorldService) {
    this.cpt = this.helloWorldService.cpt;

    this.sliderValues = [];
    for (let i = 0; i < 2; i++) {
      this.sliderValues.push({
        value: this.randomValue
      })
    }
  }

  public callService(): void {
    this.helloWorldService.incrementCpt();
  }

  private get randomValue(): number {
    return Math.random() * 100;
  }

}
