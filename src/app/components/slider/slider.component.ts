import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';

@Component({
  selector: 'app-slider',
  standalone: true,
  imports: [MatSliderModule, FormsModule],
  templateUrl: './slider.component.html',
  styleUrl: './slider.component.scss'
})
export class SliderComponent {

  @Input()
  public sliderValue: number = 0;

  @Output()
  public sliderValueChange = new EventEmitter<number>();

  public emitNewValue(newValue: any): void {
    // Émission de la valeur via l'Output
    this.sliderValueChange.emit(newValue);

    console.log("Nouvelle valeur", newValue);
  }
}
