import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appConfirm]',
  standalone: true
})
export class ConfirmDirective {

  @Input()
  public question?: string;

  @Output()
  public confirmed = new EventEmitter<void>();

  constructor() { }

  @HostListener('click')
  public onClick(): void {
    if (confirm(this.question)) {
      this.confirmed.emit();
    }
  }
}
