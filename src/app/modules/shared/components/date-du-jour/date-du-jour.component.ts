import { Component } from '@angular/core';

@Component({
  selector: 'app-date-du-jour',
  templateUrl: './date-du-jour.component.html',
  styleUrl: './date-du-jour.component.scss'
})
export class DateDuJourComponent {
  public date = new Date();
}
