import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DateDuJourComponent } from './components/date-du-jour/date-du-jour.component';
import { PadStartPipe } from '../../pipes/pad-start.pipe';
import { ConfirmDirective } from '../../directives/confirm.directive';

@NgModule({
  imports: [CommonModule, PadStartPipe, ConfirmDirective],
  exports: [CommonModule, DateDuJourComponent, PadStartPipe, ConfirmDirective],
  declarations: [
    DateDuJourComponent
  ]
})
export class SharedModule { }
